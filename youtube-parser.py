from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

videos = ""

class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.content = ""
        self.title = ""
        self.link = ""
   def startElement (self, name, attrs):  # Se ejecuta cada vez que empieza una etiqueta
        if name == 'entry':
            self.inEntry = True
        elif self.inEntry:
            if name == 'title':  # indica que lo que va a venir es de interes
                self.inContent = True
            elif name == 'link':  # indica que lo que va a venir es de interes
                self.link = attrs.get('href')

    def endElement (self, name):  # se ejecuta cuando reconoce un elemento terminal
        global videos

        if name == 'entry':
            self.inEntry = False
            videos = videos \
                     + "    <li><a href='" + self.link + "'>" \
                     + self.title + "</a></li>\n"
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
                self.content = ""
                self.inContent = False
	def characters (self, chars):
		if self.inContent:
		    self.content = self.content + chars

	Parser = make_parser()
	Parser.setContentHandler(YTHandler())

	if __name__ == "__main__":

	    PAGE = """
	<!DOCTYPE html>
	<html lang="en">
	  <body>
	    <h1>Channel contents:</h1>
	    <ul>
		{videos}
	    </ul>
	  </body>
	</html>
	"""
	
	if len(sys.argv) < 2:
        print("Usage: python3 ytbparser.py <channel id>")
        print(" <channel id>: identificador del canal")
        sys.exit(1)

    xmlFile = urllib.request.urlopen('https://www.youtube.com/feeds/videos.xml?hannel_id=' + sys.argv[1])
    Parser.parse(xmlFile)
    page = PAGE.format(videos=videos)
    print(page)
